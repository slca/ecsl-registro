��    �      �    ,      �  z   �  1     ^   >  q   �  "     �   2  �   �  0   �  C   �  �   �  T   �  3   �  f   *  �   �  G   c     �     �     �     �                *     @     T     j     n     �  F   �  c   �     8     I     i     r  '        �     �     �     �     �            c   3  
   �     �     �     �  �   �  �   �     F     M     Y     p     �     �     �     �  /   �     �     �  5   �     *     0     7  
   K     V     f  	   |     �     �     �     �     �  %   �               ,     <  	   P     Z     c     j     r     x     }  
   �     �     �     �  	   �  
   �     �     �     �     �               $     )  +   <     h     o  ?   �     �     �     �     �     �                 )      H      Y      _      x      �      �   :   �      �      �   '   !     5!     >!     N!     l!     �!     �!     �!     �!     �!     �!     �!  	   �!     �!  &   "     *"  "   ="  $   `"     �"     �"  .   �"  s   �"     F#     N#  B   c#  |   �#  s   #$  I   �$  +   �$  P   %  )   ^%  ,   �%  /   �%     �%     �%      &     &      &     ,&  
   5&     @&     T&     Z&     b&  *   i&  l   �&  ,   '  =   .'  #   l'  G   �'  F   �'  9   (     Y(     r(     �(     �(     �(     �(     �(  6   �(  \   �(  :   C)     ~)     �)     �)  .   �)  2   �)     *     ,*     D*  (   b*     �*     �*     �*  
   �*     �*     �*     �*     �*     �*     +     +     +     "+     )+     0+     7+     D+  h   J+  `  �+  �   -  .   �-  W   �-  r   *.  '   �.  �   �.  �   r/  6   20  X   i0  �   �0  K   u1  @   �1  p   2  �   s2  R   "3     u3     �3  $   �3      �3     �3     �3     4     4     %4     C4     K4     e4  F   l4  }   �4     15     E5  	   e5     o5  ,   w5  !   �5     �5     �5     �5     �5     6     )6  a   I6     �6  
   �6  
   �6     �6  �   �6  �   �7     �8     �8     �8     �8     �8     �8     �8     �8  /   �8     /9     ;9     U9     s9     z9     �9     �9     �9     �9     �9     �9     �9     �9     :     *:     ::     V:     k:     x:     �:  	   �:     �:     �:     �:     �:     �:     �:  	   �:     ;     ;     ;  	   &;  	   0;     :;     K;     X;     _;     x;     �;  	   �;     �;  3   �;     �;     �;  6   <     N<     `<     p<     x<     �<      �<      �<  *   �<     =  	   =     =     /=     ==     F=  F   M=  %   �=     �=      �=     �=     �=  '   >     3>     K>     T>     f>  !   �>     �>     �>     �>     �>     �>  %   �>     �>  -   ?  &   ;?     b?     v?  .   �?  s   �?     +@     7@  V   J@  �   �@  �   *A  E   �A  0   �A  J   (B  0   sB  ;   �B  ?   �B      C     %C     @C     WC     kC     zC     �C     �C     �C     �C     �C  #   �C  i   �C  )   MD  L   wD     �D  N   �D  T   ,E  1   �E     �E  )   �E     �E     �E     F     
F     F  H   F  s   dF  6   �F     G  #   G     7G  =   SG  /   �G     �G     �G  "   �G  5   H     SH     oH     �H     �H     �H     �H  
   �H     �H  
   �H     �H     �H     �H     I     I     I     I      I  h   'I     m   k   $   D   .   �   �   �   s   B   �   8   	       z   ;   K   �   �   W           u          �       a   -   �         �   +       4   �       �   �             @      g   R               i   h   >   U      T       �   0   �   l   �          1       S           �   �   )   {              6   �           �   F   �   &   �       `   �   '   b       "           =   M   �   �       Z   X   �       �   ,   N   �       Q          �   H       (   r             ?           E           e       �      �       �      V       }   3   t   �          v       :      [       �       �   �   �   <   �   y          �   �      !   C   �       �       7   �   �   9   �       �       �      /   �   ^   q   J   �       *   \   c   �   L   �           P   d   p   I   G          �       j   Y           O                            �      �   �   �   _   x   
   �   �   �       �   �           2   �           �   |   �           �   �   o   �           �           �   �   A   �   �   �   5          %       n   �   w   ]   �                       #   �   �   �   �   �   �   f   �   ~   �        
    Forgot your password? Enter your email in the form below and we'll send you instructions for creating a new one.
     
    Sincerely,
    %(site_name)s Management
     
    The following user (%(user)s) has asked to register an account at
    %(site_name)s.
     
    To activate this account, please click the following link within the next
    %(expiration_days)s days:
     
    To approve this, please 
     
    We have sent you an email with a link to reset your password. Please check
    your email and click the link to continue.
     
    You (or someone pretending to be you) have asked to register an account at
    %(site_name)s.  If this wasn't you, please ignore this email
    and your address will be removed from our records.
     
    Your account is now approved. You can 
     
  We have sent an email to %(email)s with further instructions.
   
Dear user:

Thanks for register into the Central American Free Software Meeting.
In order to continue with the registration process, please follow the following link:
 
The following user (%(user)s) has asked to register an account at
%(site_name)s.  
 
To approve this, please click the following link.
 
To reset your password, please click the following link, or copy and paste it
into your web browser:
 
You are receiving this email because you (or someone pretending to be you)
requested that your password be reset on the %(domain)s site. If you do not
wish to reset your password, please ignore this message.
 
Your account is now approved. You can log in using the following link
 %s (%d minutes) Account Activated Account Activation Error Account Activation Resent Account Approval failed. Account Approved Account activation on Account approval on Activation email sent Add Additional observations Agenda Alguna otra observación sobre sus gustos, usos, costumbres y manías. An account activation email will be sent shortly. Please check your email to activate your account. Approval Failure Are you sure you want to delete Audience Best regards Central American Free Software Meeting  Certificate Footer Image Certificate Header Image Change password Close Community Contributions Condiciones de Salud Confirm password reset Congratulations, your registration has been completed successfully, please enroll into the speeches Contact Us Create Room Create Scholarship Current Dear user, we want to inform you that the scholarship application period for ECSL %(ecsl)s %(year)s is active from %(start)s to %(end)s.Please follow this link to submit your scholarship application: Dear user, we want to inform you that the speech proposals period for ECSL %(ecsl)s %(year)s is active from %(start)s to %(end)s.Please follow this link to the speech from request: Delete Description Dirección en su país Duration (in minutes) Edit Edit Agenda Email Email Event Encuentros anteriores en los que ha participado End Date End Date Proposal Enter your new password below to reset your password: Event Events Fecha de Nacimiento First name First organizer Forgot your password? Greetings Género Having trouble logging in? Height 70px, Width 800px Id de transacción Identificación Identificación en su país/pasaporte Inscription Payment Institución Invalid Package Is Special Activity Last name Location Log in Log out Login Logo Logout successful Management Map Maximum Inscriptions Message My agenda My profile My speeches Nacionalidad Name New Special Activity New Speech Topic New Speech Type Next No results matched No time available in the requested position Nombre Notes for audience Once a site administrator activates your account you can login. Opción de envío Opción de pago Page Paquete Password changed Password reset Password reset complete Password successfully changed! Pay with paypal. Phone Please consider going to Presentation Previous Profile Profile created successfully, please register in the event Profile updated successfully Proposal Speech Period ECSL-{} Reasons for applying to the scholarship Register Register Failed Register updated successfully Register with Paypal. Registration Registration Complete Registration is closed Resend Activation Email Reset password Room Save Save Room Scholarship Scholarship Application Period ECSL-{} Scholarship Detail Scholarship application end period Scholarship application start period Second organizer Set password Si marcó otro indique el país de procedencia Si tiene algún comentario y/o si quiere colaborar con la organización del ECSL por favor comente en este espacio. Sign Up Skill level required Sorry, but registration is closed at this moment. Come back later. Sorry, but the scholarship application period has not been established yet. We will let you know via email when this occurs. Sorry, but the scholarship application period is not available at the time. Application period: %(start)s - %(end)s Sorry, but the scholarship application you are looking for does not exist Sorry, but there are no events at the time. Sorry, first you have to update your data and then proceed with the registration Sorry, there are no more spaces available Sorry, there is no event for you to register Sorry, we need an event to display the schedule Spaces Speaker_information Special Activities Speech Title Speech Type Speeches Start Date Start Date Proposal State Subject Submit Thanks! Your message was sent successfully The inscription payment was cancelled, please check your balance or check that you are not already enrolled. The payment is still pending to be confirmed The registration payment is done, please return to the index. The user's account is now approved. There was a problem activating your account. For more help please email There was a problem registering this user, please go to the index page There was no transaction, you already paid for this event This link will expire in Time involved in Free Software Topic Type User User: Usuario We are sorry! The proposal speech period is not active We are sorry! You have to be registered as a system user to be able to senda proposal speech We have received your scholarship application successfully Web Wrong captcha, try again You can log in. You may now <a href="%(login_url)s">log in</a> You registration is complete, this package is free Your account is now activated. Your activation key was Your password has been reset! Your username, in case you've forgotten: [CAFSM]Activation link admin approval advanced click here days. descripción everyone from %(start)s to %(end)s intermediate is Scheduled log in. logo nombre novice precio registration start ¿Tiene alguna necesidad específica de alimentación y hospedaje o alguna condición de salud especial? Project-Id-Version: ECSL V1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-06-04 17:12-0600
Last-Translator: Luis Zárate <luis.zarate@solvosoft.com>
Language-Team: ES <ES@li.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.6.10
 
    ¿Olvidó su contraseña? Ingrese su correo electrónico en el formulario y nosotros le enviaremos las instrucciones para cambiarla.
     
    Gracias,
    Equipo de %(site_name)s
     
    El siguiente usuario (%(user)s) ha solicitado registro en 
    %(site_name)s.
     
    Para activar su cuenta, por favor haga clic en el siguiente enlace, tiene 
    %(expiration_days)s dias:
     
    Para aprobar esto, por favor 
     
    Nosotros le hemos enviando un correo con el enlace para cambiar su contraseña. Por favor revise su bandeja de correo
    y haga clic en el enlace para continuar.
     
    Usted o alguien más, ha solicitado registrar su cuenta en %(site_name)s.  Si usted no ha sido, por favor ignore este correo
    y su dirección será borrada de nuestros registros.
     
    Su cuenta ahora está aprobada. Ahora puede 
     
  Hemos enviado un correo a %(email)s con instrucciones para cambiar su contraseña.
   
Estimado(a):
Gracias por registrarse en el sitio del Encuentro Centroamericano de Software Libre. 
Para continuar con el proceso de activación debe seguir el siguiente enlace:
 
El siguiente usuario (%(user)s) ha solicitado registro en 
%(site_name)s.
 
Para aprobar esto, por favor haga clic en el siguiente enlace.
 
Para reiniciar su contraseña, por favor haga click en el siguiente enlace o copie y péguelo
en su navegador:
 
Usted recibió este email porque usted (o alguien pretendiendo ser usted)
solicitó un cambio de contraseña en %(domain)s .  Si no fue usted
por favor ignore este mensaje.
 
Su cuenta está ahora activa. Usted puede iniciar sesión en el siguiente enlace
 %s (%d minutos) Cuenta activa Error en la activación de la cuenta Su reactivación ha sido enviada Validación de cuenta fallida Cuenta aprobada Cuenta activa Cuenta aprobada Correo de activación enviado Agregar Observaciones adicionales Agenda Alguna otra observación sobre sus gustos, usos, costumbres y manías. Un correo con la activación de la cuenta le llegará pronto. Por favor revise su correo electrónico para activar su cuenta. Aprobación fallida Está seguro que desea eliminar Audiencia Gracias Encuentro Centroamericano de Software Libre  Imagen pie de página certificado Imagen cabecera certificado Cambiar contraseña Cerrar Aportes a la comunidad Condiciones de Salud Confirmar cambio de contraseña Felicidades su registro se ha completado satisfactoriamente, por favor regístrese en las charlas Contáctenos Crear Sala Crear beca Evento Actual Estimado(a):
Le informamos que el período para la recepción de solicitudes de beca del ECSL %(ecsl)s %(year)s está activo
del %(start)s al %(end)s.
Siga el siguiente enlace hacia el formulario de solicitud de beca: Estimado(a):
Le informamos que el período para la recepción de propuestas del ECSL %(ecsl)s %(year)s está activo
del %(start)s al %(end)s.
Siga el siguiente enlace hacia el formulario de propuestas para charlas: Eliminar Descripción Dirección en su país Duración (en minutos) Editar Editar Agenda Correo Correo del Evento Encuentros anteriores en los que ha participado Fecha Final Fecha Final de propuestas Ingrese la nueva contraseña. Evento Eventos Fecha de Nacimiento Nombre Primer organizador ¿Olvidó su contraseña? Felicidades Género ¿Problemas al iniciar sesión? Altura 70px, Ancho 800px Id de transacción Identificación Identificación en su país Pago de inscripción Institución Paquete inválido Es actividad especial Apellidos Sede Iniciar sesión Salir Iniciar sesión Logotipo Cierre de sesión exitoso Equipo de Mapa Inscripciones máximas Mensaje Mi agenda Mi perfil Mis exposiciones Nacionalidad Nombre Nueva actividad especial Nuevo tema para charlas Nuevo tipo de charla Siguiente No hay coincidencias No hay tiempo disponible en la posición solicitada Nombre Notas para la audiencia Un administrador avaló su cuenta y ya puede loguearse Opción de envío Opción de pago Página Paquete Contraseña cambiada Reinicio de contraseña completo Reinicio de contraseña completo ¡Contraseña cambiada satisfactoriamente! Pagar con Paypal. Teléfono Considere ir al  Presentación Anterior Perfil Perfil ha sido creado satisfactoriamente, por favor registre el evento Perfil actualizado satisfactoriamente Periodo de propuestas Razones para aplicar por la beca Registro Registro Fallido Registro actualizado satisfactoriamente Registrarse con Paypal. Registro Registro Completo El registro ha sido cerrado Reenviar el correo de activación Poner contraseña Sala Guardar Guardar Sala Becas Período de solicitud de beca ECSL-{} Detalles de beca Finalización de período para solicitar beca Inicio de período para solicitar beca Segundo organizador Poner contraseña Si marcó otro indique el país de procedencia Si tiene algún comentario y/o si quiere colaborar con la organización del ECSL por favor comente en este espacio. Registrarme Nivel de habilidad Lo lamentamos, el registro está cerrado en este momento, por favor vuelva más tarde. Lo lamentamos, pero el período para solicitar beca aún no se ha establecido. Le enviaremos un correo cuando se establezcan las fechas. Lo lamentamos, pero el período para solicitar una beca no está disponible por el momento. Período para aplicar: %(start)s - %(end)s Lo lamentamos, pero la solicitud de beca que está buscando no existe Lo sentimos, pero no hay eventos por el momento. Lo lamentamos, primero actualiza tus datos y luego procede con el registro Lo lamentamos, ya no hay más espacio disponible Lo sentimos, pero no hay evento para que se pueda registrar Lo sentimos, pero necesitamos eventos para desplegar la agenda. Cupo Información del charlista Actividades especiales Titulo de la charla Tipo de charla Charlas Fecha Inicio Fecha Inicio de propuestas Estado Asunto Enviar Gracias! Mensaje enviado con éxito El pago de la inscripción fue cancelado, por favor revise su saldo o verifique que ya no está inscrito. El pago aún esta pendiente de confirmar. El pago de la inscripción está listo, favor vuelva a la página principal. La cuenta está aprobada Hubo un problema activando su cuenta. Para solicitar ayuda, envíe un correo a Hubo un problema registrando a este usuario,por favor vuelva a la página principal. No hubo transacción, ya se pagó por este evento Este enlace expirará en  Tiempo involucrado/a en el Software Libre Tema Tipo Usuario: Usuario: Usuario Lo sentimos! No estamos en periodo de envíos de propuestas para charlas Lo sentimos ! Para poder enviar una propuesta de charla tiene que estar registrado como usuario en nuestro sistema. Hemos recibido su solicitud de beca satisfactoriamente Web Captcha inválido, intente de nuevo Usted puede iniciar sesión Usted puede ahora <a href="%(login_url)s">Iniciar sesión</a> El registro está listo, el paquete es gratuito Su cuenta ahora está activa Su llave de activación era ¡Su contraseña ha sido cambiada! Su nombre de usuario en caso de que lo haya olvidado: [ECSL]Enlace de activación Administrador aprobó Avanzado haga clic aquí días. Descripción Cualquiera From %(start)s to %(end)s Intermedio Agendado iniciar sesión. Logo Nombre Básico precio Registro inicio ¿Tiene alguna necesidad específica de alimentación y hospedaje o alguna condición de salud especial? 